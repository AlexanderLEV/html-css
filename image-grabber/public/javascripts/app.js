document.addEventListener('DOMContentLoaded', function () {

  function httpPost(url, urlForParse) {
    return new Promise((resolve, reject) => {

      const xhr = new XMLHttpRequest();
      const body = 'url=' + urlForParse;

      xhr.open('POST', url, true);
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

      xhr.onload = function () {
        if (this.status === 200) {
          resolve(this.response);
        } else {
          const error = new Error(this.statusText);
          error.code = this.status;
          reject(error);
        }
      };

      xhr.onerror = function () {
        reject(new Error('Network error'));
      };

      xhr.send(body);
    });
  }

  const sendBtn = document.getElementById('send');
  const result = document.getElementById('result');
  const urlInput = document.getElementById('urlInput');

  sendBtn.addEventListener('click', function () {
    result.innerHTML= '';
    httpPost('http://localhost:3000', urlInput.value)
      .then(
      response => {
        const srcs = JSON.parse(response);
        let str = '';
        
        for (let i = 0; i < srcs.length; i++) {
          str += `<img src="${srcs[i]}"/></br>`
        }
        result.innerHTML = str;
      })
      .catch(reject => result.innerHTML = reject)        
  });

});