const express = require('express');
const router = express.Router();
const cherrio = require('cheerio');
const request = require('request');

router.post('/', function (req, res, next) {

  request(req.body.url, function (error, response, body) {
    let srcs = [];
    let htmlContent = body;
    let $ = cherrio.load(htmlContent);

    $('img').map(function () {
      let link = $(this).attr('src');
      if (typeof link === 'string' && link.startsWith('http')) {
        srcs.push(link);
      }
    });
    
    res.send(srcs);
  });
});

module.exports = router;
